package j11.cookbook;

public class Engine {

    private int horsePower;

    public void setHorsePower(int horsePower) {
        this.horsePower = horsePower;
    }

    public int getHorsePower(){
        return this.horsePower;
    }
}
