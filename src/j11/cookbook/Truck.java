package j11.cookbook;

public class Truck extends Vehicle {
    private int payload;

    public Truck(int payloadPounds, int weightPounds, int horsePower) {
        super(weightPounds + payloadPounds, horsePower);
        this.payload = payloadPounds;
    }

    public int getPayload() {
        return this.payload;
    }

    public int getMaxWeightPounds() {
        return this.weightPounds + this.payload;
    }
}

