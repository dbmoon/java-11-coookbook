package j11.cookbook;

public class Car extends Vehicle {
    private int passengersCount;

    public Car(int passengersCount, int weightPounds, int horsePower) {
        super(weightPounds + passengersCount * 250, horsePower);
        this.passengersCount = passengersCount;
    }

    public int getPassengersCount() {
        return this.passengersCount;
    }

    public int getMaxWeightPounds() {
        return this.weightPounds + this.passengersCount * 250;
    }
}
